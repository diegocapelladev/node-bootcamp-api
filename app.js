const express = require('express')
const fs = require('fs')
const bodyParser = require('body-parser')

const app = express()

app.use(express.static('public'))
app.use(bodyParser.urlencoded({ extended: true }))

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/signup.html')
})

app.post('/', (req, res) => {
  const { firstName, lastName, email } = req.body

  if (res.statusCode === 200) {
    res.send(`Successfully subscribe! Name: ${firstName + ' ' + lastName} Email: ${email}`)
  } else {
    res.send('There was an Error with signing up, please try again.')
  }
})

app.listen(5000, () => {
  console.log('Server is running.')
})